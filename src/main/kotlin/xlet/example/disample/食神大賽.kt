@file:Suppress("NonAsciiCharacters")

package xlet.example.disample

import xlet.example.disample.food.食材贊助商
import xlet.example.disample.school.中國廚藝訓練學院
import javax.inject.Inject

class 食神大賽 @Inject constructor(
    school: 中國廚藝訓練學院,
    val vendor: 食材贊助商
) {
    val cooker: StevenChow = school.出師()

    fun 冠軍料理() = run {
        val egg: 糖心荷包蛋 = cooker.火雲掌(vendor.生蛋Factory())
        val meat = vendor.醃肉Factory(醬料.美乃滋)
        val fork = vendor.叉子Factory()
        val raw: 生叉燒肉 = fork + meat
        val barbecuedPork: 叉燒肉 = cooker.火雲掌(raw)
        黯然消魂飯(barbecuedPork, egg, 洋蔥())
    }
}