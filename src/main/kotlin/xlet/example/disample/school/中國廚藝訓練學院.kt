@file:Suppress("NonAsciiCharacters")
package xlet.example.disample.school

import xlet.example.disample.StevenChow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class 中國廚藝訓練學院 @Inject constructor() {
    fun 出師() = StevenChow()
}