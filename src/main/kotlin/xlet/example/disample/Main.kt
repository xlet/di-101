@file:Suppress("NonAsciiCharacters")

package xlet.example.disample

import javax.inject.Inject

class Main {
    @Inject
    lateinit var game: 食神大賽

    init {
        DaggerGameComponent.create()
            .inject(this)
    }
}

fun main(args: Array<String>) {
    val main = Main() // entry point
    val game = main.game
    println("太好吃拉 ${game.冠軍料理()}")
}

abstract class 醃肉 {
    abstract var sauce: 醬料
}

class 自製醃肉1 : 醃肉() {
    override lateinit var sauce: 醬料

    fun sauceSetter(s: 醬料) {
        sauce = s
    }
}

class 自製醃肉2(
    override var sauce: 醬料
) : 醃肉()

class StevenChow {
    fun 火雲掌(target: 生蛋): 糖心荷包蛋 = target.magic()

    fun 火雲掌(target: 生叉燒肉): 叉燒肉 = target.magic()
}

data class 黯然消魂飯(val m: 叉燒肉, val e: 糖心荷包蛋, val v: 洋蔥)

class 叉子

class 洋蔥

enum class 醬料 { 醬油, 糖醋, 美乃滋 }

class 生叉燒肉

class 叉燒肉

class 生蛋

class 糖心荷包蛋

// region magic
inline fun <reified T, reified R> T.magic(): R {
    return R::class.java.newInstance()
}

operator inline fun <T, R, reified V> T.plus(r: R): V {
    return V::class.java.newInstance()
}
// endregion
