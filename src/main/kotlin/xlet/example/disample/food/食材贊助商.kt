@file:Suppress("NonAsciiCharacters")
package xlet.example.disample.food

import xlet.example.disample.叉子
import xlet.example.disample.生蛋
import xlet.example.disample.自製醃肉2
import xlet.example.disample.醬料
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class 食材贊助商 @Inject constructor() {
    fun 生蛋Factory() = 生蛋()

    fun 叉子Factory() = 叉子()

    fun 醃肉Factory(sauce: 醬料) = 自製醃肉2(sauce)
}