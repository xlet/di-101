package xlet.example.disample

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component
interface GameComponent {
    fun inject(main: Main)
}